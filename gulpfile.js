const gulp = require('gulp')
const pug2html = require('./gulp/pug2html')
const styles = require('./gulp/styles')
const scripts = require('./gulp/scripts')
const images = require('./gulp/images')
const server = require('browser-sync').create()
const livereload = require('gulp-livereload')
const fonts = require('./gulp/fonts')

function readyReload(cb) {
  server.reload()
  cb()
}

function serve(cb) {
  server.init({
    server: 'build',
    notify: false,
    open: true,
    cors: true
  })
  livereload.listen();
  gulp.watch('src/**/*.pug', gulp.series(pug2html))
  gulp.watch('src/**/*.scss', gulp.series(styles))
  gulp.watch('src/**/*.js', gulp.series(scripts))
  gulp.watch('src/img/*', gulp.series(images))
  gulp.watch('src/fonts/*', gulp.series(fonts))
  return cb()
}

// gulp.task('start',gulp.series(gulp.parallel(pug2html,styles)))
gulp.task('start', gulp.parallel(images,fonts,pug2html,styles,scripts,serve))
