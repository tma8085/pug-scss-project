$(document).ready(() => {
    $(document).on('click', function (e) {
        if ($(e.target).closest('.hMenu').length!=0) {
            $('.hMenu').each(function(){
                $(this).children('ul').removeClass('open')
            })
            const child = $(e.target).closest('.hMenu')[0]
            $(child).children('ul').toggleClass('open')
        }else{
            $('.hMenu').each(function(){
                $(this).children('ul').removeClass('open')
            })
        }
        e.stopPropagation();
    });
})