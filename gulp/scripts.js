const gulp = require('gulp')
const plumber = require('gulp-plumber')
const babel = require('gulp-babel')
const terser = require('gulp-terser')
const concat = require('gulp-concat')

module.exports = function pug2html() {
  return gulp.src(['node_modules/jquery/dist/jquery.js','src/**/*.js'])
    .pipe(plumber())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(concat('script.js'))
    .pipe(terser())
    .pipe(gulp.dest('build/js'))
}